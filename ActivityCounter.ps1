  <#
  .SYNOPSIS
   
      Adaptation of Key logger to count keypresses and mouse clicks to send usage data back to PRTG
      
      Author: Jordan Burch jordan@yobo.dev, tleforge tleforge@corp.richweb.com
      Based on: key logger written by Jessie Davis and hydrajump https://github.com/PowerShellMafia/PowerSploit/blob/master/Exfiltration/Get-Keystrokes.ps1
      License: BSD 3-Clause
      Required Dependencies: None
      Optional Dependencies: None

  #>


# remove cached variables, if any
Remove-Variable * -ErrorAction SilentlyContinue; Remove-Module *; $error.Clear();
# change these for your own needs
$probeip = "192.168.1.1"
$prtg_api_url = "https://192.168.1.1/api/"
$portnumber = 5050
$groupid = 12345
# start by storing the user and passhash for use in the api call
$user = "user"
$passhash = 12345678 
# this is the id of the Default Device to clone later
$clonedeviceid = 123
$clonesensorid = 124
# timer that tells how long the program should run. Recommended is 5 minutes
$timeout = new-timespan -Minutes 5 -Seconds 0

# here is an activity tracker from a github user Empire,
# TODO: understand it, modify it to push data to PRTG
# want it to poll keystrokes for a full 5 minutes (or maybe
# just 5 seconds for testing) and then push that number to
# PRTG, and then ending. The script can be called to run
# every 5 minutes by the AD server, so that's good.
function Get-Keystrokes($timeout) {

  
      [Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | Out-Null
  
      try
      {
          $ImportDll = [User32]
      }
      catch
      {
          $DynAssembly = New-Object System.Reflection.AssemblyName('Win32Lib')
          $AssemblyBuilder = [AppDomain]::CurrentDomain.DefineDynamicAssembly($DynAssembly, [Reflection.Emit.AssemblyBuilderAccess]::Run)
          $ModuleBuilder = $AssemblyBuilder.DefineDynamicModule('Win32Lib', $False)
          $TypeBuilder = $ModuleBuilder.DefineType('User32', 'Public, Class')
  
          $DllImportConstructor = [Runtime.InteropServices.DllImportAttribute].GetConstructor(@([String]))
          $FieldArray = [Reflection.FieldInfo[]] @(
              [Runtime.InteropServices.DllImportAttribute].GetField('EntryPoint'),
              [Runtime.InteropServices.DllImportAttribute].GetField('ExactSpelling'),
              [Runtime.InteropServices.DllImportAttribute].GetField('SetLastError'),
              [Runtime.InteropServices.DllImportAttribute].GetField('PreserveSig'),
              [Runtime.InteropServices.DllImportAttribute].GetField('CallingConvention'),
              [Runtime.InteropServices.DllImportAttribute].GetField('CharSet')
          )
  
          $PInvokeMethod = $TypeBuilder.DefineMethod('GetAsyncKeyState', 'Public, Static', [Int16], [Type[]] @([Windows.Forms.Keys]))
          $FieldValueArray = [Object[]] @(
              'GetAsyncKeyState',
              $True,
              $False,
              $True,
              [Runtime.InteropServices.CallingConvention]::Winapi,
              [Runtime.InteropServices.CharSet]::Auto
          )
          $CustomAttribute = New-Object Reflection.Emit.CustomAttributeBuilder($DllImportConstructor, @('user32.dll'), $FieldArray, $FieldValueArray)
          $PInvokeMethod.SetCustomAttribute($CustomAttribute)
  
          $PInvokeMethod = $TypeBuilder.DefineMethod('GetKeyboardState', 'Public, Static', [Int32], [Type[]] @([Byte[]]))
          $FieldValueArray = [Object[]] @(
              'GetKeyboardState',
              $True,
              $False,
              $True,
              [Runtime.InteropServices.CallingConvention]::Winapi,
              [Runtime.InteropServices.CharSet]::Auto
          )
          $CustomAttribute = New-Object Reflection.Emit.CustomAttributeBuilder($DllImportConstructor, @('user32.dll'), $FieldArray, $FieldValueArray)
          $PInvokeMethod.SetCustomAttribute($CustomAttribute)
  
          $PInvokeMethod = $TypeBuilder.DefineMethod('MapVirtualKey', 'Public, Static', [Int32], [Type[]] @([Int32], [Int32]))
          $FieldValueArray = [Object[]] @(
              'MapVirtualKey',
              $False,
              $False,
              $True,
              [Runtime.InteropServices.CallingConvention]::Winapi,
              [Runtime.InteropServices.CharSet]::Auto
          )
          $CustomAttribute = New-Object Reflection.Emit.CustomAttributeBuilder($DllImportConstructor, @('user32.dll'), $FieldArray, $FieldValueArray)
          $PInvokeMethod.SetCustomAttribute($CustomAttribute)
  
          $PInvokeMethod = $TypeBuilder.DefineMethod('ToUnicode', 'Public, Static', [Int32],
              [Type[]] @([UInt32], [UInt32], [Byte[]], [Text.StringBuilder], [Int32], [UInt32]))
          $FieldValueArray = [Object[]] @(
              'ToUnicode',
              $False,
              $False,
              $True,
              [Runtime.InteropServices.CallingConvention]::Winapi,
              [Runtime.InteropServices.CharSet]::Auto
          )
          $CustomAttribute = New-Object Reflection.Emit.CustomAttributeBuilder($DllImportConstructor, @('user32.dll'), $FieldArray, $FieldValueArray)
          $PInvokeMethod.SetCustomAttribute($CustomAttribute)
  
          $PInvokeMethod = $TypeBuilder.DefineMethod('GetForegroundWindow', 'Public, Static', [IntPtr], [Type[]] @())
          $FieldValueArray = [Object[]] @(
              'GetForegroundWindow',
              $True,
              $False,
              $True,
              [Runtime.InteropServices.CallingConvention]::Winapi,
              [Runtime.InteropServices.CharSet]::Auto
          )
          $CustomAttribute = New-Object Reflection.Emit.CustomAttributeBuilder($DllImportConstructor, @('user32.dll'), $FieldArray, $FieldValueArray)
          $PInvokeMethod.SetCustomAttribute($CustomAttribute)
  
          $ImportDll = $TypeBuilder.CreateType()
      }

      ### start key tracking
      $sw = [diagnostics.stopwatch]::StartNew()
      $actions = 0
      $mouseactions = 0
      while($sw.elapsed -lt $timeout) {
          Start-Sleep -Milliseconds 40
          $gotit = ""
          $Outout = ""
          
          for ($char = 1; $char -le 254; $char++) {
              $vkey = $char
              $gotit = $ImportDll::GetAsyncKeyState($vkey)
              
              if ($gotit -eq -32767) {
  
                  #check for keys not mapped by virtual keyboard
                  $LeftShift    = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::LShiftKey) -band 0x8000) -eq 0x8000
                  $RightShift   = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::RShiftKey) -band 0x8000) -eq 0x8000
                  $LeftCtrl     = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::LControlKey) -band 0x8000) -eq 0x8000
                  $RightCtrl    = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::RControlKey) -band 0x8000) -eq 0x8000
                  $LeftAlt      = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::LMenu) -band 0x8000) -eq 0x8000
                  $RightAlt     = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::RMenu) -band 0x8000) -eq 0x8000
                  $TabKey       = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Tab) -band 0x8000) -eq 0x8000
                  $DeleteKey    = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Delete) -band 0x8000) -eq 0x8000
                  $EnterKey     = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Return) -band 0x8000) -eq 0x8000
                  $BackSpaceKey = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Back) -band 0x8000) -eq 0x8000
                  $LeftArrow    = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Left) -band 0x8000) -eq 0x8000
                  $RightArrow   = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Right) -band 0x8000) -eq 0x8000
                  $UpArrow      = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Up) -band 0x8000) -eq 0x8000
                  $DownArrow    = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::Down) -band 0x8000) -eq 0x8000
                  $LeftMouse    = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::LButton) -band 0x8000) -eq 0x8000
                  $RightMouse   = ($ImportDll::GetAsyncKeyState([Windows.Forms.Keys]::RButton) -band 0x8000) -eq 0x8000
  
                  if ($LeftShift -or $RightShift) {$Outout += '[Shift]'; $actions++}
                  if ($LeftCtrl  -or $RightCtrl)  {$Outout += '[Ctrl]'; $actions++}
                  if ($LeftAlt   -or $RightAlt)   {$Outout += '[Alt]'; $actions++}
                  if ($TabKey)       {$Outout += '[Tab]'; $actions++}
                  if ($DeleteKey)    {$Outout += '[Delete]'; $actions++}
                  if ($EnterKey)     {$Outout += '[Enter]'; $actions++}
                  if ($BackSpaceKey) {$Outout += '[Backspace]'; $actions++}
                  if ($LeftArrow)    {$Outout += '[Left Arrow]'; $actions++}
                  if ($RightArrow)   {$Outout += '[Right Arrow]'; $actions++}
                  if ($UpArrow)      {$Outout += '[Up Arrow]'; $actions++}
                  if ($DownArrow)    {$Outout += '[Down Arrow]'; $actions++}
                  if ($LeftMouse)    {$Outout += '[Left Mouse]'; $mouseactions++}
                  if ($RightMouse)   {$Outout += '[Right Mouse]'; $mouseactions++}

                  #check for capslock
                  if ([Console]::CapsLock) {$Outout += '[Caps Lock]'; $actions++}
  
                  $scancode = $ImportDll::MapVirtualKey($vkey, 0x3)
                  
                  $kbstate = New-Object Byte[] 256
                  $checkkbstate = $ImportDll::GetKeyboardState($kbstate)
                  
                  $mychar = New-Object -TypeName "System.Text.StringBuilder";
                  $unicode_res = $ImportDll::ToUnicode($vkey, $scancode, $kbstate, $mychar, $mychar.Capacity, 0)
  
                  #get the title of the foreground window

                  if ($unicode_res -gt 0) {
                      $Outout += $mychar.ToString()
                      $actions++
                  }
                  Write-Host $actions
              }
          }
      }
      return $actions, $mouseactions
}

##################
# probably want to put the above function into it's own file and do things that way
# for now this will do
##################

# get the device name to determine if it needs to be added to PRTG
$name = $env:computername

# url for the initial API call
$devicesurl = $prtg_api_url + "table.json?content=devices&output=json&columns=objid,probe,group,device,host,downsens,partialdownsens,downacksens,upsens,warnsens,pausedsens,unusualsens,undefinedsens&id=" + "$groupid" + "&username=" + "$user" + "&passhash=" + "$passhash"

# Invoke the rest method to grab the json data from the url above 
# will store that as a PSCustomObject
$j = Invoke-RestMethod -Uri $devicesurl

$addurl = $prtg_api_url + "duplicateobject.htm?id=" + "$clonedeviceid" + "&name=" + "$name" + "&host=" + "$name" + "&targetid=" + "$groupid" + "&username=" + "$user" + "&passhash=" + "$passhash"

# iterate through all of the devices in the returned json (now a 
# PSCustomObject, technically) to see if the object is already in PRTG, 
# or to add it if not.
Write-Host $j.devices.Length
for($i = 0; $i -le $j.devices.Length; $i++) {
  if($j.devices[$i].device -eq $name) {
    $deviceid = $j.devices[$i].objid
    
    $findsensorurl = $prtg_api_url + "table.json?content=sensors&output=json&columns=objid,probe,group,device,sensor,status,message,lastvalue,priority,favorite&id=" + "$deviceid" + "&username=" + "$user" + "&passhash=" + "$passhash"
    $s = Invoke-RestMethod -Uri $findsensorurl
    ### would need to iterate through ALL sensors (s.treesize, or s.length?), match to a sensor name, and 
    ### THEN get the sensor id if there are expected to be more than one sensor
    $sensorid = $s.sensors[0].objid

    break
  }
  elseif ($i -eq $j.devices.Length -or $j.devices.Length -eq 0) {
    # If we make it here, then we are at the end of the list, and haven't found 
    # this desktop OR list is empty. time to add this device"
    $Result = Invoke-WebRequest -Uri $addurl -UseBasicParsing
    # $matches will be populated from the following match request  
    $surpress = $Result | Where-Object {$_ -match '\?id=(.*?)\"'}
    $deviceid = $Matches[1]

    # now to clone the sensor
    $sensorurl = $prtg_api_url + "duplicateobject.htm?id=" + "$clonesensorid" + "&name=pxf-" + "$name" + "&targetid=" + "$deviceid" + "&username=" + "$user" + "&passhash=" + "$passhash" 
    $Result = Invoke-WebRequest -Uri $sensorurl -UseBasicParsing
    # $matches will be populated from the following match request  
    $surpress = $Result | Where-Object {$_ -match '\?id=(.*?)\"'}
    $sensorid = $Matches[1]
  }
}

# need to change the sensor the token before unpausing
$token_change_url = $prtg_api_url + "setobjectproperty.htm?id=$sensorid&name=httppushtoken&value=$name&username=$user&passhash=$passhash" 
Invoke-WebRequest -Uri $token_change_url -UseBasicParsing

# need to make sure both the device AND the sensor are unpaused 
$unpausedeviceurl = $prtg_api_url + "pause.htm?id=" + "$deviceid" + "&action=1&username=" + "$user" + "&passhash=" + "$passhash"
$unpausesensorurl = $prtg_api_url + "pause.htm?id=" + "$sensorid" + "&action=1&username=" + "$user" + "&passhash=" + "$passhash"
$Result = Invoke-WebRequest -Uri $unpausedeviceurl -UseBasicParsing
$Result = Invoke-WebRequest -Uri $unpausesensorurl -UseBasicParsing

# now to use the actual function to track actions
$_totalactions = Get-Keystrokes $timeout
$actions = $_totalactions[0] + $_totalactions[1]
$apm = $actions/$timeout.TotalMinutes

#XML used to pass the data gathered
$xml =     
"<prtg>
    <result>
        <channel>TotalActionsPerMinute</channel>
        <value>" + $apm + "</value>
        <unit>Custom</unit>
        <float>1</float>
        <Mode>Absolute</Mode>
        <CustomUnit>apm</CustomUnit>
    </result>
    <result>
        <channel>KeyboardActions</channel>
        <value>" + $_totalactions[0] + "</value>
        <unit>Custom</unit>
        <Mode>Absolute</Mode>
        <CustomUnit>KeyPresses</CustomUnit>
    </result>
    <result>
        <channel>MouseActions</channel>
        <value>" + $_totalactions[1] + "</value>
        <unit>Custom</unit>
        <Mode>Absolute</Mode>
        <CustomUnit>MouseButtonPresses</CustomUnit>
    </result>
</prtg>"

# final API call to actually push all the gathered data to PRTG
$httppushurl = "http://" + "$probeip" + ":" + "$portnumber" + "/" + "$name" + "?content=" + "$xml"
$Result = Invoke-WebRequest -Uri $httppushurl -UseBasicParsing
