# PRTG HTTP Push Data Advanced Sensors
## activityCounter
The purpose of this sensor is to keep a running count of the number of key presses/mouse clicks that a user makes over an amount of time. Ideally, this script will run while a user's PC is online, and will push data constantly until it is turned off. The script will gather this information, and push the statistics of: KeyPresses, MouseClicks, and TotalActionsPerMinute to the PRTG sensor.
This sensor is intended to be deployed using Active Directory Group Policy/Windows Scheduler to be pushed out to a large group of computers. It first adds a device based off of computer's name, creates the sensor to log data against, then finally records and reports data.
It is intened to be set on the windows schedular on a 5 minute interval.

### Installation
1. Create Target Destination group in PRTG
2. Create Empty Clone Device in PRTG
3. Create Empty Clone Sensor in PRTG(HTTP PUSH Data Advanced, set it to go to unknown status after 10 minutes)
<img src="images/clone_dev_sens.png">
Image of a device with a HTTP PUSH Data Advanced Sensor set to unknown

4. Change variables in script ($probeip, $prtg_api_url, $portnumber, $groupid, $user, $passhash, $clonedeviceid, $clonesensorid) to fit your specific setup.
<img src="images/default_variables.png">
Image of a device with an HTTP PUSH Data Advanced Sensor set to unknown.

Need to get the passhash from PRTG site:
<img src="images/PRTG_passhash.png">

5. Test script on desktop, almost certainly will want to change $timeout to be something like 10 seconds instead of 5 minutes for quicker tests... (bonus note: you will probably have to put this in a secure folder and execute as Administrator to get around Active Directory Security Policies, and/or will need to disable Windows Defender/other antivirus software while testing. The functions for listening to key presses are red flagged as security vulnerabilities). Can run powershell scripts through the command line (windows cmd).
6. Deploy under windows schedular, schedule this task to run as administrator every 5 minutes while a user is logged on. For a good rundown on how to utilize windows scheduler, the following links are be good resources:

Using ADGP + task scheduler:
https://www.faqforge.com/windows-server-2016/configure-scheduled-task-item-using-group-policy/

Just using task scheduler:
https://blog.netwrix.com/2018/07/03/how-to-automate-powershell-scripts-with-task-scheduler/